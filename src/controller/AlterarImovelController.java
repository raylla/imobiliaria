package controller;

import model.Apartamento;
import model.Casa;
import model.DAO.ApartamentoDAO;
import model.DAO.CasaDAO;
import model.DAO.KitnetDAO;
import model.Kitnet;
import view.AlterarImovel;
import view.ExibirImovel;

public class AlterarImovelController {
    
    private final AlterarImovel view;

    public AlterarImovelController(AlterarImovel view) {
        this.view = view;
    }
    
    public void atulizarDados(){
        ExibirImovel imovel = ExibirImovel.getInstancia();
        
        if(imovel.retornaTipo().equals("Casa")){
        Casa casa = new Casa();
        CasaDAO cdao = new CasaDAO();
        
        casa.setTipo(view.getTxtTipo().getSelectedItem().toString());
        casa.setStatus(view.getTxtStatus().getSelectedItem().toString());
        casa.setValor(Float.parseFloat(view.getTxtValor().getText()));
        casa.setRua(view.getTxtRua().getText());
        casa.setBairro(view.getTxtBairro().getText());
        casa.setCidade(view.getTxtCidade().getText());
        casa.setNumero(view.getTxtNum().getText());
        casa.setQuartosComSuite(view.getTxtQuartosComSuite().getText());
        casa.setQuartosSemSuite(view.getTxtQuartosSemSuite().getText());
        casa.setAndares(view.getTxtAndares().getText());
        casa.setBanheiros(view.getTxtBanheiros().getText());
        casa.setArea(Float.parseFloat(view.getTxtAreaTotal().getText()));
        casa.setDescricao(view.getTxtDescricao().getText());
        casa.setIdCasa(imovel.retornaId());
        cdao.updateCasa(casa);
        
        }
        else if(imovel.retornaTipo().equals("Apartamento")){
        Apartamento apt = new Apartamento();
        ApartamentoDAO pdao = new ApartamentoDAO();
            
        apt.setTipo(view.getTxtTipo().getSelectedItem().toString());
        apt.setStatus(view.getTxtStatus().getSelectedItem().toString());
        apt.setValor(Float.parseFloat(view.getTxtValor().getText()));
        apt.setRua(view.getTxtRua().getText());
        apt.setBairro(view.getTxtBairro().getText());
        apt.setCidade(view.getTxtCidade().getText());
        apt.setNumero(view.getTxtNum().getText());
        apt.setQuartosComSuite(view.getTxtQuartosComSuite().getText());
        apt.setQuartosSemSuite(view.getTxtQuartosSemSuite().getText());
        apt.setAndares(view.getTxtAndares().getText());
        apt.setBanheiros(view.getTxtBanheiros().getText());
        apt.setArea(Float.parseFloat(view.getTxtAreaTotal().getText()));
        apt.setDescricao(view.getTxtDescricao().getText());
        apt.setIdApartamento(imovel.retornaId());
        pdao.updateApartamento(apt);
            
      } else{
        Kitnet kitnet = new Kitnet();
        KitnetDAO kdao = new KitnetDAO();
          
        kitnet.setTipo(view.getTxtTipo().getSelectedItem().toString());
        kitnet.setStatus(view.getTxtStatus().getSelectedItem().toString());
        kitnet.setValor(Float.parseFloat(view.getTxtValor().getText()));
        kitnet.setRua(view.getTxtRua().getText());
        kitnet.setBairro(view.getTxtBairro().getText());
        kitnet.setCidade(view.getTxtCidade().getText());
        kitnet.setNumero(view.getTxtNum().getText());
        kitnet.setQuartosComSuite(view.getTxtQuartosComSuite().getText());
        kitnet.setQuartosSemSuite(view.getTxtQuartosSemSuite().getText());
        kitnet.setAndares(view.getTxtAndares().getText());
        kitnet.setBanheiros(view.getTxtBanheiros().getText());
        kitnet.setArea(Float.parseFloat(view.getTxtAreaTotal().getText()));
        kitnet.setDescricao(view.getTxtDescricao().getText());
        kitnet.setIdKitnet(imovel.retornaId());
        kdao.updateKitnet(kitnet);
          
        }
    }
}
