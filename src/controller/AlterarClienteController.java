package controller;

import javax.swing.JOptionPane;
import model.Cliente;
import model.DAO.ClienteDAO;
import view.AlterarCliente;
import view.ExibirCliente;

public class AlterarClienteController {
    
    private final AlterarCliente view;

    public AlterarClienteController(AlterarCliente view) {
        this.view = view;
    }
    
    public void atualizarDados(){
        if(view.getTxtCpf().getText().trim().isEmpty() || view.getTxtNome().getText().trim().isEmpty() ||
        view.getTxtSexo().getSelectedItem().equals("Selecione") || view.getTxtRua().getText().trim().isEmpty() ||
        view.getTxtBairro().getText().trim().isEmpty() || view.getTxtCidade().getText().trim().isEmpty() ||
        view.getTxtNum().getText().trim().isEmpty() || view.getTxtTel().getText().trim().isEmpty() ||
        view.getTxtEmail().getText().trim().isEmpty() || view.getTxtDataNasc().getDate() == null){
            JOptionPane.showMessageDialog(null,"Preencha todos os campos!");
            
        } else {
            Cliente cliente = new Cliente();
            ClienteDAO cdao = new ClienteDAO();
            ExibirCliente cli = ExibirCliente.getInstancia();
            
            cliente.setNome(view.getTxtNome().getText());
            cliente.setSexo(view.getTxtSexo().getSelectedItem().toString());
            cliente.setDataNasc(view.getTxtDataNasc().getDate());
            cliente.setRua(view.getTxtRua().getText());
            cliente.setBairro(view.getTxtBairro().getText());
            cliente.setCidade(view.getTxtCidade().getText());
            cliente.setNumero(view.getTxtNum().getText());
            cliente.setTelefone(view.getTxtTel().getText());
            cliente.setEmail(view.getTxtEmail().getText());
            cliente.setCpf(cli.retornaCpf());
            cdao.updateClientes(cliente);   
            view.dispose();
        }
    }
}
