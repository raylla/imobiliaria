package controller;

import javax.swing.JOptionPane;
import view.Login;
import view.MenuPrincipal;

public class LoginController {

    private final Login view;
    
    public LoginController(Login view){
        this.view = view; 
    }
    
    public void entrarNoSistema(){
        String usuario = view.getTxtLogin().getText();
        String senha = new String (view.getTxtSenha().getPassword());
        
        if(usuario.equals("admin") && senha.equals("1234")){
            this.view.exibirMensagem("Bem vindo!");
            MenuPrincipal tela = new MenuPrincipal();
            tela.setVisible(true);
            this.view.dispose();
        } else {
            this.view.exibirMensagem("Usuário ou senha inválidos!");
        }
    }
}
