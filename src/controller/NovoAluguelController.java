package controller;

import controller.helper.ContratoHelper;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Apartamento;
import model.Casa;
import model.Cliente;
import model.Contrato;
import model.DAO.ApartamentoDAO;
import model.DAO.CasaDAO;
import model.DAO.ClienteDAO;
import model.DAO.ContratoDAO;
import model.DAO.KitnetDAO;
import model.Kitnet;
import view.NovoAluguel;

public class NovoAluguelController {
    
    private final NovoAluguel view;
    private final ContratoHelper helper;

    public NovoAluguelController(NovoAluguel view) {
        this.view = view;
        this.helper = new ContratoHelper(view);
    }
    
    public void checarDados(){
    
    if(view.getTxtValor().getText().trim().isEmpty() || view.getTxtDataInicio().getDate() == null ||
       view.getTxtDataFinal().getDate() == null || (view.getjTable1().getSelectedRow() == -1) ||
       (view.getjTable2().getSelectedRow() == -1)){
       JOptionPane.showMessageDialog(null, "Preencha todos os campos ou selecione um comprador/imovel!");
            
        } else{
            
            NovoAluguel aluguel = NovoAluguel.getInstancia();
            
            if(aluguel.retornaStatus().equals("Disponivel")){
                Contrato contrato = helper.getModelo();
                ContratoDAO cdao = new ContratoDAO();
                KitnetDAO kdao = new KitnetDAO();
                Kitnet kit = new Kitnet();
                ApartamentoDAO adao = new ApartamentoDAO();
                Apartamento apt = new Apartamento();
                CasaDAO casadao = new CasaDAO();
                Casa casa = new Casa();
               
                cdao.inserirContrato(contrato);
                if(aluguel.retornaTipo().equals("Apartamento")){
                    apt.setIdApartamento(aluguel.retornaId());
                    adao.alterarStatus(apt, "Alugado");
                    
                } else if(aluguel.retornaTipo().equals("Casa")){
                    casa.setIdCasa(aluguel.retornaId());
                    casadao.alterarStatus(casa, "Alugado");
                } else{
                    kit.setIdKitnet(aluguel.retornaId());
                    kdao.alterarStatus(kit, "Alugado");
                }
            }
            
            else{
                JOptionPane.showMessageDialog(null, "O imóvel já está alugado!!");
            }
        }
    }
    
    public void preencherJTableCliente() {
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        ClienteDAO cdao = new ClienteDAO();
        
        for(Cliente c: cdao.listarTodosClientes()){
            modelo.addRow(new Object[]{
                c.getCpf(),
                c.getNome(),
                c.getTelefone(),
                c.getEmail()
            });
                    
        }
    }
    
    
    public void preencherJTableImovel(){
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable2().getModel();
        modelo.setNumRows(0);
        CasaDAO cdao = new CasaDAO();
        KitnetDAO kdao = new KitnetDAO();
        ApartamentoDAO adao = new ApartamentoDAO();
        
        for(Apartamento a: adao.listarApartamentos()){
            modelo.addRow(new Object[]{
                a.getImovelId(),
                a.getIdApartamento(),
                a.getTipo(),
                a.getStatus(),
                a.getValor()
            });                        
        }
        
        for(Casa c: cdao.listarCasas()){
            modelo.addRow(new Object[]{
                c.getImovelId(),
                c.getIdCasa(),
                c.getTipo(),
                c.getStatus(),
                c.getValor()
            });                        
        } 
        
        for(Kitnet k: kdao.listarKitnets()){
            modelo.addRow(new Object[]{
                k.getImovelId(),
                k.getIdKitnet(),
                k.getTipo(),
                k.getStatus(),
                k.getValor()
            });                        
        }     
    }
        
}
