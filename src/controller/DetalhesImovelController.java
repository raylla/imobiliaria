package controller;

import view.DetalhesImovel;

public class DetalhesImovelController {
    private final DetalhesImovel view;

    public DetalhesImovelController(DetalhesImovel view) {
        this.view = view;
    }
    
    public void desabilitarView() {
        view.getTxtAndares().setEditable(false);
        view.getTxtArea().setEditable(false);
        view.getTxtBairro().setEditable(false);
        view.getTxtBanheiros().setEditable(false);
        view.getTxtCidade().setEditable(false);
        view.getTxtDescricao().setEditable(false);
        view.getTxtNum().setEditable(false);
        view.getTxtQuartosComSuite().setEditable(false);
        view.getTxtQuartosSemSuite().setEditable(false);
        view.getTxtRua().setEditable(false);
        view.getTxtStatus().setEnabled(false);
        view.getTxtTipo().setEnabled(false);
        view.getTxtValor().setEditable(false);
    }
}
