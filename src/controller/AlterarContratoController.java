package controller;

import javax.swing.JOptionPane;
import model.Contrato;
import model.DAO.ContratoDAO;
import view.AlterarContrato;
import view.ExibirContrato;

public class AlterarContratoController {
    
    private final AlterarContrato view;

    public AlterarContratoController(AlterarContrato view) {
        this.view = view;
    }
    
    public void atualizarDados(){
        if(view.getTxtDataInicio().getDate() == null || view.getTxtDataFinal().getDate() == null ||
                view.getTxtValor().getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!!");
            
        } else{
            Contrato contrato = new Contrato();
            ContratoDAO cdao = new ContratoDAO();
            ExibirContrato exibir = ExibirContrato.getInstancia();
            
            contrato.setDataInicio(view.getTxtDataInicio().getDate());
            contrato.setDataFinal(view.getTxtDataFinal().getDate());
            contrato.setValor(Float.parseFloat(view.getTxtValor().getText()));
            contrato.setIdContrato(exibir.retornaIdContrato()); 
            cdao.updateContrato(contrato);
            view.dispose();
           
        }
    }
}
