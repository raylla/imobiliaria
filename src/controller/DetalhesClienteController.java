package controller;

import view.DetalhesCliente;

public class DetalhesClienteController {
    private final DetalhesCliente view;

    public DetalhesClienteController(DetalhesCliente view) {
        this.view = view;
    }
    
    public void desabilitarView() {
        view.getTxtBairro3().setEditable(false);
        view.getTxtCidade3().setEditable(false);
        view.getTxtCpf3().setEditable(false);
        view.getTxtEmail3().setEditable(false);
        view.getTxtNum3().setEditable(false);
        view.getTxtRua3().setEditable(false);
        view.getTxtSexo3().setEnabled(false);
        view.getTxtTel3().setEditable(false);
        view.getTxtNome3().setEditable(false);
        view.getTxtDataNasc3().setEnabled(false);
    }
}
