package controller;

import controller.helper.CadastroClienteHelper;
import model.Cliente;
import model.DAO.ClienteDAO;
import view.CadastroCliente;

public class CadastroClienteController {
    private final CadastroCliente view;
    private final CadastroClienteHelper helper;

    public CadastroClienteController(CadastroCliente view) {
        this.view = view;
        this.helper = new CadastroClienteHelper(view);
    }
   
    public void checarCampos(){
        if(view.getTxtCpf().getText().trim().isEmpty() || view.getTxtNome().getText().trim().isEmpty() ||
           view.getTxtSexo().getSelectedItem().equals("Selecione") || view.getTxtRua().getText().trim().isEmpty() ||
           view.getTxtBairro().getText().trim().isEmpty() || view.getTxtCidade().getText().trim().isEmpty() ||
           view.getTxtNum().getText().trim().isEmpty() || view.getTxtTel().getText().trim().isEmpty() ||
           view.getTxtEmail().getText().trim().isEmpty() || view.getTxtDataNasc().getDate() == null){
           view.exibirMensagem("Preencha todos os campos!");
            
        }
        else{
            Cliente cliente = helper.getModelo();
            ClienteDAO clienteDao = new ClienteDAO();
        
            clienteDao.inserirCliente(cliente);
            helper.limparTela();
        }
    }
}
