package controller;

import controller.helper.CadastroImovelHelper;
import model.Apartamento;
import model.Casa;
import model.DAO.ApartamentoDAO;
import model.DAO.CasaDAO;
import model.DAO.KitnetDAO;
import model.Kitnet;
import view.CadastroImovel;

public class CadastroImovelController {
    private final CadastroImovel view;
    private final CadastroImovelHelper helper;

    public CadastroImovelController(CadastroImovel view) {
        this.view = view;
        this.helper = new CadastroImovelHelper(view);
    }
    
    public void checarDados(){
        if(view.getTxtTipo().getSelectedItem().equals("Selecione") || view.getTxtStatus().getSelectedItem().equals("Selecione")
           || view.getTxtValor().getText().trim().isEmpty() || view.getTxtRua().getText().trim().isEmpty() ||
           view.getTxtCidade().getText().trim().isEmpty() || view.getTxtBairro().getText().trim().isEmpty() ||
           view.getTxtNum().getText().trim().isEmpty() || view.getTxtQuartosComSuite().getText().trim().isEmpty() ||
           view.getTxtQuartosSemSuite().getText().trim().isEmpty() || view.getTxtNumAndares().getText().trim().isEmpty() ||
           view.getTxtNumBanheiros().getText().trim().isEmpty() || view.getTxtAreaTotal().getText().trim().isEmpty() ||
           view.getTxtDescricao().getText().trim().isEmpty()) {
           view.exibirMensagem("Preencha todos os campos!");
            
        } else{
            if(view.getTxtTipo().getSelectedItem().equals("Apartamento")){
                Apartamento apartamento = helper.getModeloApartamento();
                ApartamentoDAO pdao = new ApartamentoDAO();
                
                pdao.inserirApartamento(apartamento);
            } 
            else if(view.getTxtTipo().getSelectedItem().equals("Casa")){
                Casa casa = helper.getModeloCasa();
                CasaDAO casaDao = new CasaDAO();
                
                casaDao.inserirCasa(casa);
                
            } else{
                Kitnet kitnet = helper.getModeloKitnet();
                KitnetDAO kdao = new KitnetDAO();
                
                kdao.inserirKitnet(kitnet);
            }
            
            helper.limpaTela();
        }
    }
}
