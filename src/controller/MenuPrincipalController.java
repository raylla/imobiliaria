package controller;

import view.NovoAluguel;
import view.CadastroCliente;
import view.CadastroImovel;
import view.ExibirCliente;
import view.ExibirContrato;
import view.ExibirImovel;
import view.MenuPrincipal;

public class MenuPrincipalController {
    private final MenuPrincipal view;
    
    public MenuPrincipalController(MenuPrincipal view){
        this.view = view;
    }
    
    public void navegarCadastroCliente() {
        CadastroCliente cadastroCliente = new CadastroCliente();
        cadastroCliente.setVisible(true);
    }
    
    public void navegarCadastroImovel() {
        CadastroImovel cadastroImovel = new CadastroImovel();
        cadastroImovel.setVisible(true);
    }
    
    public void navegarExibirCliente() {
        ExibirCliente exibirCliente = new ExibirCliente();
        ExibirCliente.setInstancia(exibirCliente);
        exibirCliente.setVisible(true);
    }
    
    public void navegarExibirImovel() {
        ExibirImovel exibirImovel = new ExibirImovel();
        ExibirImovel.setInstancia(exibirImovel);
        exibirImovel.setVisible(true);
    }
    
    public void navegarExibirContrato() {
        ExibirContrato exibirContrato = new ExibirContrato();
        exibirContrato.setInstancia(exibirContrato);
        exibirContrato.setVisible(true);
    }
    
    public void navegarAlugar() {
        NovoAluguel alugar = new NovoAluguel();
        NovoAluguel.setInstancia(alugar);
        alugar.setVisible(true);
    }
    
    public void navegarSair() {
        System.exit(0);
    }
}
