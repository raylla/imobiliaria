package controller;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Apartamento;
import model.Casa;
import model.Contrato;
import model.DAO.ApartamentoDAO;
import model.DAO.CasaDAO;
import model.DAO.ContratoDAO;
import model.DAO.KitnetDAO;
import model.Kitnet;
import view.AlterarContrato;
import view.ExibirContrato;

public class ExibirContratoController {
    private final ExibirContrato view;

    public ExibirContratoController(ExibirContrato view) {
        this.view = view;
    }
    
    public void navegarAlterarContrato(){
        
        if(view.getjTable1().getSelectedRow() != -1){
            AlterarContrato alterarContrato = new AlterarContrato();
            alterarContrato.setVisible(true);
            
        } else{
            JOptionPane.showMessageDialog(null, "Selecione um campo para alterar!");
        }
    }
    
    public void preencherJTable(){
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        ContratoDAO cdao = new ContratoDAO();
        
        for(Contrato c: cdao.listarContratos()){
            modelo.addRow(new Object[]{
                c.getIdContrato(),
                c.getImovelId(),
                c.getClienteCpf(),
                c.getDataInicio(),
                c.getDataFinal(),
                c.getValor()
            });
        }
    }
    
    public void excluir(){
        
        if(view.getjTable1().getSelectedRow() != -1){
            Contrato contrato = new Contrato();
            ContratoDAO cdao = new ContratoDAO();
            
            ExibirContrato exibir = ExibirContrato.getInstancia();
            Casa casa = new Casa();
            CasaDAO csdao = new CasaDAO();
            Kitnet kitnet = new Kitnet();
            KitnetDAO kdao = new KitnetDAO();
            Apartamento apt = new Apartamento();
            ApartamentoDAO adao = new ApartamentoDAO();
            
            String tipo = view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 1).toString();
            
            if(tipo.equals("Apartamento")){
                contrato.setIdContrato((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                cdao.deleteContrato(contrato);
               // apt.setImovelId(exibir.retornaIdEspecifico());
               // adao.alterarStatus(apt, "Disponivel");
                
            } else if(tipo.equals("Casa")){
                contrato.setIdContrato((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                cdao.deleteContrato(contrato);
                //casa.setImovelId(exibir.retornaIdEspecifico());
                //csdao.alterarStatus(casa, "Disponivel");
                  
            } else{
                contrato.setIdContrato((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                cdao.deleteContrato(contrato);
                //kitnet.setImovelId(exibir.retornaIdEspecifico());
                //kdao.alterarStatus(kitnet, "Disponivel");
            }
            
        } else{
            
        }
    }
}
