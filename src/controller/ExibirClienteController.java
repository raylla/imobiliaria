package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Cliente;
import model.DAO.ClienteDAO;
import view.AlterarCliente;
import view.DetalhesCliente;
import view.ExibirCliente;

public class ExibirClienteController {
    private final ExibirCliente view;
    
    public ExibirClienteController(ExibirCliente view){
        this.view = view;
    }
    
    public void navegarDetalhesCliente(){
        if(view.getjTable1().getSelectedRow() != -1){
            DetalhesCliente detalhesCliente = new DetalhesCliente();
            DetalhesCliente.setInstancia(detalhesCliente);
            detalhesCliente.setVisible(true);
            
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um campo para exibir!");
        }
    }
    
    public void navegarAlterarCliente(){
        if(view.getjTable1().getSelectedRow() != -1){
            AlterarCliente alterarCliente = new AlterarCliente();
            alterarCliente.setVisible(true);
            
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um campo para alterar!");
        }
    }
    
    public void preencherJTable() {
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        ClienteDAO cdao = new ClienteDAO();
        
        for(Cliente c: cdao.listarTodosClientes()){
            modelo.addRow(new Object[]{
                c.getCpf(),
                c.getNome(),
                c.getTelefone(),
                c.getEmail()
            });
                    
        }
    }
    
    public void exibirPesquisa(String texto) {
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        ClienteDAO cdao = new ClienteDAO();
        
        for(Cliente c: cdao.exibirPesquisa(texto)){
            modelo.addRow(new Object[]{
                c.getCpf(),
                c.getNome(),
                c.getTelefone(),
                c.getEmail()
            });
                    
        }
    }
    
    public void excluir() {
        
        if(view.getjTable1().getSelectedRow() != -1){
            
            Cliente cliente = new Cliente();
            ClienteDAO cdao = new ClienteDAO();
            
            cliente.setCpf(view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0).toString());
            cdao.deletarCliente(cliente);   
            
        } else {
            JOptionPane.showMessageDialog(null, "Selecione um campo para excluir!");
        }
    }   
    
}
