package controller.helper;

import java.util.Date;
import model.Cliente;
import view.CadastroCliente;

public class CadastroClienteHelper {
    private final CadastroCliente view;

    public CadastroClienteHelper(CadastroCliente view) {
        this.view = view;
    }
    
    public Cliente getModelo(){
        
        String cpf = view.getTxtCpf().getText();
        String nome = view.getTxtNome().getText();
        Date dataNasc = view.getTxtDataNasc().getDate();
        String sexo = (String) view.getTxtSexo().getSelectedItem();
        String rua = view.getTxtRua().getText();
        String bairro = view.getTxtBairro().getText();
        String cidade = view.getTxtCidade().getText();
        String numero = view.getTxtNum().getText();
        String telefone = view.getTxtTel().getText();
        String email = view.getTxtEmail().getText();
        
       Cliente modelo = new Cliente.ClienteBuilder().cpf(cpf).nome(nome).dataNasc(dataNasc)
               .sexo(sexo).rua(rua).bairro(bairro).cidade(cidade).numero(numero).telefone(telefone)
               .email(email).criarCliente();
        
        return modelo;
    }   
    
    public void setModelo(Cliente modelo) {
        String cpf = modelo.getCpf();
        String nome = modelo.getNome();
        Date dataNasc = view.getTxtDataNasc().getDate();
        String sexo = modelo.getSexo();
        String rua = modelo.getRua();
        String bairro = modelo.getBairro();
        String cidade = modelo.getCidade();
        String numero = modelo.getNumero();
        String telefone = modelo.getTelefone();
        String email = modelo.getEmail();
        
        view.getTxtCpf().setText(cpf);
        view.getTxtNome().setText(nome);
        view.getTxtDataNasc().setDate(dataNasc);
        view.getTxtSexo().setSelectedItem(sexo);
        view.getTxtRua().setText(rua);
        view.getTxtBairro().setText(bairro);
        view.getTxtCidade().setText(cidade);
        view.getTxtNum().setText(numero);
        view.getTxtTel().setText(telefone);
        view.getTxtEmail().setText(email);
    }
    
    public void limparTela() {
        view.getTxtCpf().setText("");
        view.getTxtNome().setText("");
        view.getTxtDataNasc().setDateFormatString("");
        view.getTxtSexo().setSelectedItem("Selecione");
        view.getTxtRua().setText("");
        view.getTxtBairro().setText("");
        view.getTxtCidade().setText("");
        view.getTxtNum().setText("");
        view.getTxtTel().setText("");
        view.getTxtEmail().setText("");
    }
}
