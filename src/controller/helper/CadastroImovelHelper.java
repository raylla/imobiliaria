package controller.helper;

import model.Apartamento;
import model.Casa;
import model.Kitnet;
import view.CadastroImovel;

public class CadastroImovelHelper {
    private final CadastroImovel view;

    public CadastroImovelHelper(CadastroImovel view) {
        this.view = view;
    }
    
    public Casa getModeloCasa(){
        
        String tipo = (String) view.getTxtTipo().getSelectedItem();
        String status = (String) view.getTxtStatus().getSelectedItem();
        float valor = Float.parseFloat(view.getTxtValor().getText());
        String rua = view.getTxtRua().getText();
        String cidade = view.getTxtCidade().getText();
        String bairro = view.getTxtBairro().getText();
        String numero = view.getTxtNum().getText();
        String quartosComSuite = view.getTxtQuartosComSuite().getText();
        String quartosSemSuite = view.getTxtQuartosSemSuite().getText();
        String andares = view.getTxtNumAndares().getText();
        String banheiros = view.getTxtNumBanheiros().getText();
        float area = Float.parseFloat(view.getTxtAreaTotal().getText());
        String descricao = view.getTxtDescricao().getText();
        
        Casa modelo = new Casa(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,quartosSemSuite,
        andares,banheiros,area,descricao);
        
        return modelo;
    }
    
    public Apartamento getModeloApartamento(){
        
        String tipo = (String) view.getTxtTipo().getSelectedItem();
        String status = (String) view.getTxtStatus().getSelectedItem();
        float valor = Float.parseFloat(view.getTxtValor().getText());
        String rua = view.getTxtRua().getText();
        String cidade = view.getTxtCidade().getText();
        String bairro = view.getTxtBairro().getText();
        String numero = view.getTxtNum().getText();
        String quartosComSuite = view.getTxtQuartosComSuite().getText();
        String quartosSemSuite = view.getTxtQuartosSemSuite().getText();
        String andares = view.getTxtNumAndares().getText();
        String banheiros = view.getTxtNumBanheiros().getText();
        float area = Float.parseFloat(view.getTxtAreaTotal().getText());
        String descricao = view.getTxtDescricao().getText();
        
        Apartamento modelo = new Apartamento(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,
        quartosSemSuite,andares,banheiros,area,descricao);
        
        return modelo;
    }
    
    public Kitnet getModeloKitnet(){
        
        String tipo = (String) view.getTxtTipo().getSelectedItem();
        String status = (String) view.getTxtStatus().getSelectedItem();
        float valor = Float.parseFloat(view.getTxtValor().getText());
        String rua = view.getTxtRua().getText();
        String cidade = view.getTxtCidade().getText();
        String bairro = view.getTxtBairro().getText();
        String numero = view.getTxtNum().getText();
        String quartosComSuite = view.getTxtQuartosComSuite().getText();
        String quartosSemSuite = view.getTxtQuartosSemSuite().getText();
        String andares = view.getTxtNumAndares().getText();
        String banheiros = view.getTxtNumBanheiros().getText();
        float area = Float.parseFloat(view.getTxtAreaTotal().getText());
        String descricao = view.getTxtDescricao().getText();
        
        Kitnet modelo = new Kitnet(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,quartosSemSuite,
        andares,banheiros,area,descricao);
        
        return modelo;
    }
    
    public void limpaTela() {
        view.getTxtTipo().setSelectedItem("Selecione");
        view.getTxtStatus().setSelectedItem("Selecione");
        view.getTxtValor().setText("");
        view.getTxtRua().setText("");
        view.getTxtCidade().setText("");
        view.getTxtBairro().setText("");
        view.getTxtNum().setText("");
        view.getTxtQuartosComSuite().setText("");
        view.getTxtQuartosSemSuite().setText("");
        view.getTxtNumAndares().setText("");
        view.getTxtNumBanheiros().setText("");
        view.getTxtAreaTotal().setText("");
        view.getTxtDescricao().setText("");
    }
    
}
