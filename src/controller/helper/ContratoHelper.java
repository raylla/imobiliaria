package controller.helper;
import java.util.Date;
import model.Contrato;
import view.NovoAluguel;

public class ContratoHelper {
    
    private final NovoAluguel view;

    public ContratoHelper(NovoAluguel view) {
        this.view = view;
    }
    
    public Contrato getModelo(){
        
        Date dataInicio = view.getTxtDataInicio().getDate();
        Date dataFinal = view.getTxtDataFinal().getDate();
        float valor = Float.parseFloat(view.getTxtValor().getText());
        String clienteCpf = (String) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0);
        int imovelId = (int) view.getjTable2().getValueAt(view.getjTable2().getSelectedRow(), 0);
        
        Contrato modelo = new Contrato.ContratoBuilder().dataInicio(dataInicio)
                .dataFinal(dataFinal).valor(valor).ClienteCpf(clienteCpf)
                .ImovelId(imovelId).criarContrato();
        
        return modelo;
    }
}
