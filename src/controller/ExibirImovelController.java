package controller;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.Apartamento;
import model.Casa;
import model.DAO.ApartamentoDAO;
import model.DAO.CasaDAO;
import model.DAO.KitnetDAO;
import model.Kitnet;
import view.AlterarImovel;
import view.DetalhesImovel;
import view.ExibirImovel;

public class ExibirImovelController {
    private final ExibirImovel view;

    public ExibirImovelController(ExibirImovel view) {
        this.view = view;
    }
    
    public void navegarAlterarImovel() {
        if(view.getjTable1().getSelectedRow() != -1){
            AlterarImovel alterarImovel = new AlterarImovel();
            
            alterarImovel.setVisible(true);
            
        } else{
            JOptionPane.showMessageDialog(null, "Selecione um campo para alterar!");
        }
    }
    
    public void navegarDetalhesImovel() {
        if(view.getjTable1().getSelectedRow() != -1){
            DetalhesImovel detalhesImovel = new DetalhesImovel();
            detalhesImovel.setVisible(true);
            
        } else{
            JOptionPane.showMessageDialog(null, "Selecione um campo para exibir!");
        }
    }
    
    public void preencherJTable(){
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        CasaDAO cdao = new CasaDAO();
        KitnetDAO kdao = new KitnetDAO();
        ApartamentoDAO adao = new ApartamentoDAO();
        
        for(Apartamento a: adao.listarApartamentos()){
            modelo.addRow(new Object[]{
                a.getImovelId(),
                a.getIdApartamento(),
                a.getTipo(),
                a.getStatus(),
                a.getValor()
            });                        
        }    
        
        for(Casa c: cdao.listarCasas()){
            modelo.addRow(new Object[]{
                c.getImovelId(),
                c.getIdCasa(),
                c.getTipo(),
                c.getStatus(),
                c.getValor()
            });                        
        } 
        
        for(Kitnet k: kdao.listarKitnets()){
            modelo.addRow(new Object[]{
                k.getImovelId(),
                k.getIdKitnet(),
                k.getTipo(),
                k.getStatus(),
                k.getValor()
            });                        
        } 
    }
    
    public void excluir(){
        if(view.getjTable1().getSelectedRow() != -1){
            
            Casa casa = new Casa();
            CasaDAO cdao = new CasaDAO();
            Kitnet kitnet = new Kitnet();
            KitnetDAO kdao = new KitnetDAO();
            Apartamento apt = new Apartamento();
            ApartamentoDAO adao = new ApartamentoDAO();
            
            String tipo = view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 1).toString();
            
            if(tipo.equals("Casa")){
                casa.setIdCasa((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                cdao.deletarCasa(casa);
            }
            else if(tipo.equals("Apartamento")){
                apt.setIdApartamento((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                adao.deletarApartamento(apt);
            }
            else{
                kitnet.setIdKitnet((int) view.getjTable1().getValueAt(view.getjTable1().getSelectedRow(), 0));
                kdao.deletarKitnet(kitnet);
            }
        } else{
            JOptionPane.showMessageDialog(null, "Selecione um campo para excluir!");
        }
    }
    
    
    public void exibirPesquisa(String texto){
        DefaultTableModel modelo = (DefaultTableModel) view.getjTable1().getModel();
        modelo.setNumRows(0);
        CasaDAO cdao = new CasaDAO();
        KitnetDAO kdao = new KitnetDAO();
        ApartamentoDAO adao = new ApartamentoDAO();
        
         for(Apartamento a: adao.exibirPesquisaApartamento(texto)){
            modelo.addRow(new Object[]{
                a.getIdApartamento(),
                a.getTipo(),
                a.getStatus(),
                a.getValor()
            });                        
        }
        
        for(Casa c: cdao.exibirPesquisaCasa(texto)){
            modelo.addRow(new Object[]{
                c.getIdCasa(),
                c.getTipo(),
                c.getStatus(),
                c.getValor()
            });                        
        } 
        
        for(Kitnet k: kdao.exibirPesquisaKitnet(texto)){
            modelo.addRow(new Object[]{
                k.getIdKitnet(),
                k.getTipo(),
                k.getStatus(),
                k.getValor()
            });                        
        }     
    }
}
