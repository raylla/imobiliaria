package model;

public class Casa extends Imovel {
    private String descricao;
    private int idCasa;

    public Casa(String tipo, String status, float valor, String rua, String bairro, 
    String cidade, String numero, String quartosComSuite, String quartosSemSuite, 
    String andares, String banheiros, float area,String descricao) {
        
        super(tipo, status, valor, rua, bairro, cidade, numero, quartosComSuite, quartosSemSuite, 
        andares, banheiros, area);
        
        this.descricao = descricao;
    }

    public Casa() {
        
    }
    
    public static class CasaBuilder extends Imovel{
        private String descricao;
        private int idCasa;

        public CasaBuilder() {
        }
        
        public CasaBuilder descricao(String descricao){
            this.descricao = descricao;
            return this;
        }
    }
    
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }   

    public int getIdCasa() {
        return idCasa;
    }

    public void setIdCasa(int idCasa) {
        this.idCasa = idCasa;
    }
    
}
