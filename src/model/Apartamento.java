package model;

public class Apartamento extends Imovel {
    private String descricao;
    private int idApartamento;

    public Apartamento(String tipo, String status, float valor, String rua, String bairro, 
    String cidade, String numero, String quartosComSuite, String quartosSemSuite, 
    String andares, String banheiros, float area, String descricao) {
        
        super(tipo, status, valor, rua, bairro, cidade, numero, quartosComSuite, quartosSemSuite, 
        andares, banheiros, area);
        
        this.descricao = descricao;
    }

    public Apartamento() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdApartamento() {
        return idApartamento;
    }

    public void setIdApartamento(int idApartamento) {
        this.idApartamento = idApartamento;
    }
    
    

}
