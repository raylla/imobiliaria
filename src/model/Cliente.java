package model;

import java.util.Date;


public class Cliente {
    private String cpf;
    private String nome;
    private String sexo;
    private Date dataNasc;
    private String rua;
    private String bairro;
    private String cidade;
    private String numero;
    private String telefone;
    private String email;

    public Cliente() {
    }

    public Cliente(String cpf, String nome, String sexo, Date dataNasc, String rua, String bairro, 
    String cidade, String numero, String telefone, String email) {
        this.cpf = cpf;
        this.nome = nome;
        this.sexo = sexo;
        this.dataNasc = dataNasc;
        this.rua = rua;
        this.bairro = bairro;
        this.cidade = cidade;
        this.numero = numero;
        this.telefone = telefone;
        this.email = email;
    }
    
    public static class ClienteBuilder {
        private String cpf;
        private String nome;
        private String sexo;
        private Date dataNasc;
        private String rua;
        private String bairro;
        private String cidade;
        private String numero;
        private String telefone;
        private String email;

        public ClienteBuilder() {
        }
        
        public ClienteBuilder cpf(String cpf){
            this.cpf = cpf;
            return this;
        }
        
        public ClienteBuilder nome(String nome){
            this.nome = nome;
            return this;
        }
        
        public ClienteBuilder sexo(String sexo){
            this.sexo = sexo;
            return this;
        }
        
        public ClienteBuilder dataNasc(Date dataNasc){
            this.dataNasc = dataNasc;
            return this;
        }
        
        public ClienteBuilder rua(String rua){
            this.rua = rua;
            return this;
        }
        
        public ClienteBuilder bairro(String bairro){
            this.bairro = bairro;
            return this;
        }
        
        public ClienteBuilder cidade(String cidade){
            this.cidade = cidade;
            return this;
        }
        
        public ClienteBuilder numero(String numero){
            this.numero = numero;
            return this;
        }
        
        public ClienteBuilder telefone(String telefone){
            this.telefone = telefone;
            return this;
        }
        
        public ClienteBuilder email(String email){
            this.email = email;
            return this;
        }
        
        public Cliente criarCliente(){
            return new Cliente(cpf,nome,sexo,dataNasc,rua,bairro,cidade,numero,telefone,email);
        }
    }
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(Date dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
