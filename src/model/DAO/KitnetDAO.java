package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Kitnet;

public class KitnetDAO {
    
    public void inserirKitnet(Kitnet kitnet){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO imobiliaria.Kitnet "
            + "(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,quartosSemSuite,andares,"
            + "banheiros,areatotal,descricao) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            ps.setString(1, kitnet.getTipo());
            ps.setString(2, kitnet.getStatus());
            ps.setFloat(3, kitnet.getValor());
            ps.setString(4, kitnet.getRua());
            ps.setString(5, kitnet.getBairro());
            ps.setString(6, kitnet.getCidade());
            ps.setString(7, kitnet.getNumero());
            ps.setString(8, kitnet.getQuartosComSuite());
            ps.setString(9, kitnet.getQuartosSemSuite());
            ps.setString(10, kitnet.getAndares());
            ps.setString(11, kitnet.getBanheiros());
            ps.setFloat(12, kitnet.getArea());
            ps.setString(13, kitnet.getDescricao());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        }
    }
    
    
    public List<Kitnet> listarKitnets(){
        List<Kitnet> kitnets = new ArrayList<>();
        
        try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Kitnet");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Kitnet kitnet = new Kitnet();
                
                kitnet.setImovelId(rs.getInt("idImovel"));
                kitnet.setIdKitnet(rs.getInt("idKitnet"));
                kitnet.setTipo(rs.getString("tipo"));
                kitnet.setStatus(rs.getString("status"));
                kitnet.setValor(rs.getFloat("valor"));
                kitnets.add(kitnet);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return kitnets;
    }
    
    public void deletarKitnet(Kitnet kitnet){
         int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM imobiliaria.Kitnet WHERE IdKitnet = (?)");
            
            ps.setInt(1, kitnet.getIdKitnet());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível deletar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na deleção! " +ex.getMessage());
        } 
     }
    
    public Kitnet preencherAlterarImovel(int filtroId){
        Kitnet retorno = new Kitnet();
        
        try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade, "
             + "numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + "FROM imobiliaria.Kitnet WHERE idkitnet = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
    }
    
    
    public Kitnet preencherDetalhesImovel(int filtroId){
        Kitnet retorno = new Kitnet();
        
        try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade, "
             + "numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + "FROM imobiliaria.Kitnet WHERE idkitnet = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
    }
    
    
    public void updateKitnet(Kitnet kitnet){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.kitnet "
            + "SET tipo = (?),status = (?),valor = (?),rua = (?),bairro = (?),cidade = (?), "
            + "numero = (?),quartosComSuite = (?),quartosSemSuite = (?),andares = (?),"
            + "banheiros = (?),areatotal = (?),descricao = (?) WHERE idkitnet = (?)");
            
            ps.setString(1, kitnet.getTipo());
            ps.setString(2, kitnet.getStatus());
            ps.setFloat(3, kitnet.getValor());
            ps.setString(4, kitnet.getRua());
            ps.setString(5, kitnet.getBairro());
            ps.setString(6, kitnet.getCidade());
            ps.setString(7, kitnet.getNumero());
            ps.setString(8, kitnet.getQuartosComSuite());
            ps.setString(9, kitnet.getQuartosSemSuite());
            ps.setString(10, kitnet.getAndares());
            ps.setString(11, kitnet.getBanheiros());
            ps.setFloat(12, kitnet.getArea());
            ps.setString(13, kitnet.getDescricao());
            ps.setInt(14, kitnet.getIdKitnet());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }
    
    
    public List<Kitnet> exibirPesquisaKitnet(String texto){
        List<Kitnet> kitnets = new ArrayList<>();
        
        try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Kitnet "
            + " WHERE tipo LIKE (?)");
            ps.setString(1, "%" +texto +"%");
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Kitnet kitnet = new Kitnet();
                
                kitnet.setIdKitnet(rs.getInt("idKitnet"));
                kitnet.setTipo(rs.getString("tipo"));
                kitnet.setStatus(rs.getString("status"));
                kitnet.setValor(rs.getFloat("valor"));
                kitnets.add(kitnet);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return kitnets;
    }
    
    
    public void alterarStatus(Kitnet kitnet, String texto){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.kitnet "
            + "SET status = (?) WHERE idkitnet = (?)");
            
            ps.setString(1, texto);
            ps.setInt(2, kitnet.getIdKitnet());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }

}
