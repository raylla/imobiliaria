package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Contrato;

public class ContratoDAO {
    
    public void inserirContrato(Contrato contrato){
    int res = 0;   
    try {
        Connection connection = DAOFactory.getConexao();
        PreparedStatement ps = connection.prepareStatement("INSERT INTO imobiliaria.Contrato "
        + " (datainicio,datafinal,valor,clientecpf,imovelid) VALUES (?,?,?,?,?)");
            
            ps.setDate(1, new Date (contrato.getDataInicio().getTime()));
            ps.setDate(2, new Date (contrato.getDataFinal().getTime()));
            ps.setDouble(3, contrato.getValor());
            ps.setString(4, contrato.getClienteCpf());
            ps.setInt(5, contrato.getImovelId());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Inserido com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível inserir!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        } 
    }
    
    public List<Contrato> listarContratos(){
        List<Contrato> contratos = new ArrayList<>();
        
        try{
            Connection connection = DAOFactory.getConexao();
            ResultSet rs = null;
            
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Contrato");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Contrato contrato = new Contrato();
                
                contrato.setIdContrato(rs.getInt("idcontrato"));
                contrato.setImovelId(rs.getInt("imovelid"));
                contrato.setClienteCpf(rs.getString("clientecpf"));
                contrato.setDataInicio(rs.getDate("datainicio"));
                contrato.setDataFinal(rs.getDate("datafinal"));
                contrato.setValor(rs.getDouble("valor"));
                contratos.add(contrato);
                
            }
            
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        }
        return contratos;
    }
    
    public void updateContrato(){
        
    }
    
    
    public Contrato preencherAlterarContrato(int filtroId){
        Contrato retorno = new Contrato();
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("SELECT dataInicio,dataFinal,valor"
                    + " FROM imobiliaria.Contrato WHERE idContrato = (?)");
            
            ps.setInt(1, filtroId);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                Date dataInicio = rs.getDate(1);
                retorno.setDataInicio(dataInicio);
                Date dataFinal = rs.getDate(2);
                retorno.setDataFinal(dataFinal);
                Double valor = rs.getDouble(3);
                retorno.setValor(valor);
            }
            return retorno;
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        }
    }
    
    
    public void updateContrato(Contrato contrato){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.Contrato SET dataInicio = (?),"
            + "dataFinal = (?),valor = (?) WHERE idContrato = (?)");
            
            ps.setDate(1, new Date (contrato.getDataInicio().getTime()));
            ps.setDate(2, new Date (contrato.getDataFinal().getTime()));
            ps.setDouble(3, contrato.getValor());
            ps.setInt(4, contrato.getIdContrato());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível atualizar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        } 
    }
    
    
    public void deleteContrato(Contrato contrato){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM imobiliaria.Contrato "
                    + " WHERE idContrato = (?)");
            
            ps.setInt(1, contrato.getIdContrato());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível deletar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na deleção! " +ex.getMessage());
        } 
    }

}
