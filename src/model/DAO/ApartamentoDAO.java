package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Apartamento;

public class ApartamentoDAO {
    
    public void inserirApartamento(Apartamento apartamento){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO imobiliaria.Apartamento "
            + "(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,quartosSemSuite,andares,"
            + "banheiros,areatotal,descricao) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            ps.setString(1, apartamento.getTipo());
            ps.setString(2, apartamento.getStatus());
            ps.setFloat(3, apartamento.getValor());
            ps.setString(4, apartamento.getRua());
            ps.setString(5, apartamento.getBairro());
            ps.setString(6, apartamento.getCidade());
            ps.setString(7, apartamento.getNumero());
            ps.setString(8, apartamento.getQuartosComSuite());
            ps.setString(9, apartamento.getQuartosSemSuite());
            ps.setString(10, apartamento.getAndares());
            ps.setString(11, apartamento.getBanheiros());
            ps.setFloat(12, apartamento.getArea());
            ps.setString(13, apartamento.getDescricao());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        }
    }
    
    
    public List<Apartamento> listarApartamentos(){
        List<Apartamento> apartamentos = new ArrayList<>();
        
        try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Apartamento");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Apartamento apartamento = new Apartamento();
                
                apartamento.setImovelId(rs.getInt("idImovel"));
                apartamento.setIdApartamento(rs.getInt("idApartamento"));
                apartamento.setTipo(rs.getString("tipo"));
                apartamento.setStatus(rs.getString("status"));
                apartamento.setValor(rs.getFloat("valor"));
                apartamentos.add(apartamento);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return apartamentos;
    }
    
    public void deletarApartamento(Apartamento apt){
         int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM imobiliaria.Apartamento WHERE IdApartamento = (?)");
            
            ps.setInt(1, apt.getIdApartamento());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível deletar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na deleção! " +ex.getMessage());
        } 
     }
    
    public Apartamento preencherAlterarImovel(int filtroId){
         Apartamento retorno = new Apartamento();
         
         try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade,"
            + " numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + " FROM imobiliaria.Apartamento WHERE idApartamento = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
     }
    
    
    public Apartamento preencherDetalhesImovel(int filtroId){
         Apartamento retorno = new Apartamento();
         
         try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade,"
            + " numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + " FROM imobiliaria.Apartamento WHERE idApartamento = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
     }
    
    
    public void updateApartamento(Apartamento apt){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.apartamento "
            + "SET tipo = (?),status = (?),valor = (?),rua = (?),bairro = (?),cidade = (?), "
            + "numero = (?),quartosComSuite = (?),quartosSemSuite = (?),andares = (?),"
            + "banheiros = (?),areatotal = (?),descricao = (?) WHERE idapartamento = (?)");
            
            ps.setString(1, apt.getTipo());
            ps.setString(2, apt.getStatus());
            ps.setFloat(3, apt.getValor());
            ps.setString(4, apt.getRua());
            ps.setString(5, apt.getBairro());
            ps.setString(6, apt.getCidade());
            ps.setString(7, apt.getNumero());
            ps.setString(8, apt.getQuartosComSuite());
            ps.setString(9, apt.getQuartosSemSuite());
            ps.setString(10, apt.getAndares());
            ps.setString(11, apt.getBanheiros());
            ps.setFloat(12, apt.getArea());
            ps.setString(13, apt.getDescricao());
            ps.setInt(14, apt.getIdApartamento());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }
    
    
    public List<Apartamento> exibirPesquisaApartamento(String texto){
        List<Apartamento> apartamentos = new ArrayList<>();
        
        try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Apartamento "
            + " WHERE tipo LIKE (?)");
            ps.setString(1, "%" +texto +"%");
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Apartamento apartamento = new Apartamento();
                
                apartamento.setIdApartamento(rs.getInt("idApartamento"));
                apartamento.setTipo(rs.getString("tipo"));
                apartamento.setStatus(rs.getString("status"));
                apartamento.setValor(rs.getFloat("valor"));
                apartamentos.add(apartamento);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return apartamentos;
    }
    
    public void alterarStatus(Apartamento apt, String texto){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.apartamento "
            + "SET status = (?) WHERE idapartamento = (?)");
            
            ps.setString(1, texto);
            ps.setInt(2, apt.getIdApartamento());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }

}
