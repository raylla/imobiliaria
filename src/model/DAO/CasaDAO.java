package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Casa;

public class CasaDAO {
    
     public void inserirCasa(Casa casa){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO imobiliaria.Casa (tipo,status,valor,"
            + "rua,bairro,cidade,numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao) "
            + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            ps.setString(1, casa.getTipo());
            ps.setString(2, casa.getStatus());
            ps.setDouble(3, casa.getValor());
            ps.setString(4, casa.getRua());
            ps.setString(5, casa.getBairro());
            ps.setString(6, casa.getCidade());
            ps.setString(7, casa.getNumero());
            ps.setString(8, casa.getQuartosComSuite());
            ps.setString(9, casa.getQuartosSemSuite());
            ps.setString(10, casa.getAndares());
            ps.setString(11, casa.getBanheiros());
            ps.setFloat(12, casa.getArea());
            ps.setString(13, casa.getDescricao());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        }
     }
     
     
     public List<Casa> listarCasas(){
         List<Casa> casas = new ArrayList<>();
         
         try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Casa");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Casa casa = new Casa();
                
                casa.setImovelId(rs.getInt("idImovel"));
                casa.setIdCasa(rs.getInt("idCasa"));
                casa.setTipo(rs.getString("tipo"));
                casa.setStatus(rs.getString("status"));
                casa.setValor(rs.getFloat("valor"));
                casas.add(casa);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
         
         return casas;
     }
     
     public void deletarCasa(Casa casa){
         int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM imobiliaria.Casa WHERE IdCasa = (?)");
            
            ps.setInt(1, casa.getIdCasa());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível deletar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na deleção! " +ex.getMessage());
        } 
     }
     
     public Casa preencherAlterarImovel(int filtroId){
         Casa retorno = new Casa();
         
         try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade, "
             + "numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + "FROM imobiliaria.Casa WHERE idcasa = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
     }
     
     public Casa preencherDetalhesImovel(int filtroId){
         Casa retorno = new Casa();
         
         try{
             Connection connection = DAOFactory.getConexao();
             PreparedStatement ps = connection.prepareStatement("SELECT tipo,status,valor,rua,bairro,cidade, "
             + "numero,quartosComSuite,quartosSemSuite,andares,banheiros,areatotal,descricao "
             + "FROM imobiliaria.Casa WHERE idcasa = (?)");
             
             ps.setInt(1, filtroId);
             
             ResultSet rs = ps.executeQuery();
             
             if(rs.next()){
                String tipo = rs.getString(1);
                retorno.setTipo(tipo);
                String status = rs.getString(2);
                retorno.setStatus(status);
                float valor = rs.getFloat(3);
                retorno.setValor(valor);
                String rua = rs.getString(4);
                retorno.setRua(rua);
                String bairro = rs.getString(5);
                retorno.setBairro(bairro);
                String cidade = rs.getString(6);
                retorno.setCidade(cidade);
                String numero = rs.getString(7);
                retorno.setNumero(numero); 
                String quartosComSuite = rs.getString(8);
                retorno.setQuartosComSuite(quartosComSuite);
                String quartosSemSuite = rs.getString(9);
                retorno.setQuartosSemSuite(quartosSemSuite);
                String andares = rs.getString(10);
                retorno.setAndares(andares);
                String banheiros = rs.getString(11);
                retorno.setBanheiros(banheiros);
                float areaTotal = rs.getFloat(12);
                retorno.setArea(areaTotal);
                String descricao = rs.getString(13);
                retorno.setDescricao(descricao);
             }
             
             return retorno;
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
    }
     
     
    public void updateCasa(Casa casa){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.casa "
            + "SET tipo = (?),status = (?),valor = (?),rua = (?),bairro = (?),cidade = (?), "
            + "numero = (?),quartosComSuite = (?),quartosSemSuite = (?),andares = (?),"
            + "banheiros = (?),areatotal = (?),descricao = (?) WHERE idcasa = (?)");
            
            ps.setString(1, casa.getTipo());
            ps.setString(2, casa.getStatus());
            ps.setFloat(3, casa.getValor());
            ps.setString(4, casa.getRua());
            ps.setString(5, casa.getBairro());
            ps.setString(6, casa.getCidade());
            ps.setString(7, casa.getNumero());
            ps.setString(8, casa.getQuartosComSuite());
            ps.setString(9, casa.getQuartosSemSuite());
            ps.setString(10, casa.getAndares());
            ps.setString(11, casa.getBanheiros());
            ps.setFloat(12, casa.getArea());
            ps.setString(13, casa.getDescricao());
            ps.setInt(14, casa.getIdCasa());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }
    
     public List<Casa> exibirPesquisaCasa(String texto){
         List<Casa> casas = new ArrayList<>();
         
         try{
             Connection connection = DAOFactory.getConexao();
             ResultSet rs = null;
             
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Casa "
            + " WHERE tipo LIKE (?)");
            ps.setString(1, "%" +texto +"%");
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Casa casa = new Casa();
                
                casa.setIdCasa(rs.getInt("idCasa"));
                casa.setTipo(rs.getString("tipo"));
                casa.setStatus(rs.getString("status"));
                casa.setValor(rs.getFloat("valor"));
                casas.add(casa);
                
            }
             
         } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
         
         return casas;
     }
     
     public void alterarStatus(Casa casa, String texto){
        int res = 0;
        
        try{
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.casa "
            + "SET status = (?) WHERE idcasa = (?)");
            
            ps.setString(1, texto);
            ps.setInt(2,casa.getIdCasa());
            
            res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        }
    }
}
