package model.DAO;

import connection.DAOFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Cliente;

public class ClienteDAO {
    
    public void inserirCliente(Cliente cliente){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("INSERT INTO imobiliaria.cliente (cpf,nome,sexo,"
                    + "dataNasc,rua,bairro,cidade,numero,telefone,email) VALUES (?,?,?,?,?,?,?,?,?,?)");
            
            ps.setString(1, cliente.getCpf());
            ps.setString(2, cliente.getNome());
            ps.setString(3, cliente.getSexo());
            ps.setDate(4, new Date (cliente.getDataNasc().getTime()));
            ps.setString(5, cliente.getRua());
            ps.setString(6, cliente.getBairro());
            ps.setString(7, cliente.getCidade());
            ps.setString(8, cliente.getNumero());
            ps.setString(9, cliente.getTelefone());
            ps.setString(10, cliente.getEmail());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível salvar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na inserção! " +ex.getMessage());
        } 
    }
    
    public void deletarCliente(Cliente cliente) {
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("DELETE FROM imobiliaria.Cliente WHERE Cpf = (?)");
            
            ps.setString(1, cliente.getCpf());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível deletar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na deleção! " +ex.getMessage());
        } 
    }
    
    public List<Cliente> listarTodosClientes(){
        List<Cliente> clientes = new ArrayList<>();
        
        try{ 
            Connection connection = DAOFactory.getConexao();
            ResultSet rs = null;
        
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Cliente");
            rs = ps.executeQuery();
        
            while(rs.next()){
            
                Cliente cliente = new Cliente();
                
                cliente.setCpf(rs.getString("cpf"));
                cliente.setNome(rs.getString("nome"));
                cliente.setTelefone(rs.getString("telefone"));
                cliente.setEmail(rs.getString("email"));
                clientes.add(cliente);
            }
        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return clientes;
    }
    
    
    public Cliente preencherAlterarCliente(String filtroCPF){
        Cliente retorno = new Cliente();
        
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("SELECT cpf,nome,sexo,dataNasc,rua,bairro,"
                    + "cidade,numero,telefone,email FROM imobiliaria.Cliente WHERE Cpf = (?)");
            
        ps.setString(1, filtroCPF);
        
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            String cpf = rs.getString(1);
            retorno.setCpf(cpf);
            String nome = rs.getString(2);
            retorno.setNome(nome);
            String sexo = rs.getString(3);
            retorno.setSexo(sexo);
            Date dataNasc = rs.getDate(4);
            retorno.setDataNasc(dataNasc);
            String rua = rs.getString(5);
            retorno.setRua(rua);
            String bairro = rs.getString(6);
            retorno.setBairro(bairro);
            String cidade = rs.getString(7);
            retorno.setCidade(cidade);
            String numero = rs.getString(8);
            retorno.setNumero(numero);
            String telefone = rs.getString(9);
            retorno.setTelefone(telefone);
            String email = rs.getString(10);
            retorno.setEmail(email);
        }
        return retorno;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
    }
    
    
    public Cliente preencherDetalhesCliente(String filtroCPF){
        Cliente retorno = new Cliente();
        
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("SELECT cpf,nome,sexo,dataNasc,rua,bairro,"
                    + "cidade,numero,telefone,email FROM imobiliaria.Cliente WHERE Cpf = (?)");
            
        ps.setString(1, filtroCPF);
        
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            String cpf = rs.getString(1);
            retorno.setCpf(cpf);
            String nome = rs.getString(2);
            retorno.setNome(nome);
            String sexo = rs.getString(3);
            retorno.setSexo(sexo);
            Date dataNasc = rs.getDate(4);
            retorno.setDataNasc(dataNasc);
            String rua = rs.getString(5);
            retorno.setRua(rua);
            String bairro = rs.getString(6);
            retorno.setBairro(bairro);
            String cidade = rs.getString(7);
            retorno.setCidade(cidade);
            String numero = rs.getString(8);
            retorno.setNumero(numero);
            String telefone = rs.getString(9);
            retorno.setTelefone(telefone);
            String email = rs.getString(10);
            retorno.setEmail(email);
            
        }
        return retorno;
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao preencher! " +ex.getMessage());
            return null;
        } 
    }
    
    public void updateClientes(Cliente cliente){
        int res = 0;
        try {
            Connection connection = DAOFactory.getConexao();
            PreparedStatement ps = connection.prepareStatement("UPDATE imobiliaria.Cliente SET nome = (?),"
            + "sexo = (?),dataNasc = (?),rua = (?),bairro = (?),cidade = (?),numero = (?),telefone = (?),"
            + "email = (?) WHERE cpf = (?)");
            
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getSexo());
            ps.setDate(3, new Date (cliente.getDataNasc().getTime()));
            ps.setString(4, cliente.getRua());
            ps.setString(5, cliente.getBairro());
            ps.setString(6, cliente.getCidade());
            ps.setString(7, cliente.getNumero());
            ps.setString(8, cliente.getTelefone());
            ps.setString(9, cliente.getEmail());
            ps.setString(10, cliente.getCpf());
            
        res = ps.executeUpdate();
            
        if (res > 0){
        JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
        }
        else{
        JOptionPane.showMessageDialog(null, "Não foi possível atualizar!");    
        }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na atualização! " +ex.getMessage());
        } 
    }
    
    
    public List<Cliente> exibirPesquisa(String texto){
        List<Cliente> clientes = new ArrayList<>();
        
        try{ 
            Connection connection = DAOFactory.getConexao();
            ResultSet rs = null;
        
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM imobiliaria.Cliente "
            + " WHERE nome LIKE (?)");
            ps.setString(1, "%" +texto +"%");
            
            rs = ps.executeQuery();
        
            while(rs.next()){
            
                Cliente cliente = new Cliente();
                
                cliente.setCpf(rs.getString("cpf"));
                cliente.setNome(rs.getString("nome"));
                cliente.setTelefone(rs.getString("telefone"));
                cliente.setEmail(rs.getString("email"));
                clientes.add(cliente);
            }
        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro na listagem! " +ex.getMessage());
        } 
        
        return clientes;
    }
    
}