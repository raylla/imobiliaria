package model;
import java.util.Date;

public class Contrato {
    private int idContrato;
    private Date dataInicio;
    private Date dataFinal;
    private double valor;
    private String ClienteCpf;
    private int ImovelId;

    public Contrato(Date dataInicio, Date dataFinal, double valor, String ClienteCpf, int ImovelId) {
        this.dataInicio = dataInicio;
        this.dataFinal = dataFinal;
        this.valor = valor;
        this.ClienteCpf = ClienteCpf;
        this.ImovelId = ImovelId;
    }

    public Contrato() {
        
    }
    
    public static class ContratoBuilder {
        private Date dataInicio;
        private Date dataFinal;
        private double valor;
        private String ClienteCpf;
        private int ImovelId;

        public ContratoBuilder() {
        }
        
        public ContratoBuilder dataInicio(Date dataInicio){
            this.dataInicio = dataInicio;
            return this;
        }
        
        public ContratoBuilder dataFinal(Date dataFinal){
            this.dataFinal = dataFinal;
            return this;
        }
        
        public ContratoBuilder valor(double valor){
            this.valor = valor;
            return this;
        }
        
        public ContratoBuilder ClienteCpf(String ClienteCpf){
            this.ClienteCpf = ClienteCpf;
            return this;
        }
        
        public ContratoBuilder ImovelId(int ImovelId){
            this.ImovelId = ImovelId;
            return this;
        }
        
        public Contrato criarContrato(){
            return new Contrato(dataInicio,dataFinal,valor,ClienteCpf,ImovelId);
        }
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getClienteCpf() {
        return ClienteCpf;
    }

    public void setClienteCpf(String ClienteCpf) {
        this.ClienteCpf = ClienteCpf;
    }

    public int getImovelId() {
        return ImovelId;
    }

    public void setImovelId(int ImovelId) {
        this.ImovelId = ImovelId;
    }

    public int getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(int idContrato) {
        this.idContrato = idContrato;
    }
    
}
