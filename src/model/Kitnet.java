package model;

public class Kitnet extends Imovel {
    private String descricao;
    private int idKitnet;

    public Kitnet(String tipo, String status, float valor, String rua, String bairro, 
    String cidade, String numero, String quartosComSuite, String quartosSemSuite, 
    String andares, String banheiros, float area,String descricao) {
        
        super(tipo, status, valor, rua, bairro, cidade, numero, quartosComSuite, quartosSemSuite, 
        andares, banheiros, area);
        
        this.descricao = descricao;
    }

    public Kitnet() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdKitnet() {
        return idKitnet;
    }

    public void setIdKitnet(int idKitnet) {
        this.idKitnet = idKitnet;
    }
    
    

}
