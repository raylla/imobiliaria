package model;

public abstract class Imovel {
    private int ImovelId;
    private String tipo;
    private String status;
    private float valor;         
    private String rua;
    private String bairro;
    private String cidade;
    private String numero;
    private String quartosComSuite;
    private String quartosSemSuite;
    private String andares;
    private String banheiros;
    private float area;

    public Imovel(String tipo, String status, float valor, String rua, String bairro, String cidade, String numero, 
    String quartosComSuite, String quartosSemSuite, String andares, String banheiros, float area) {
        this.tipo = tipo;
        this.status = status;
        this.valor = valor;
        this.rua = rua;
        this.bairro = bairro;
        this.cidade = cidade;
        this.numero = numero;
        this.quartosComSuite = quartosComSuite;
        this.quartosSemSuite = quartosSemSuite;
        this.andares = andares;
        this.banheiros = banheiros;
        this.area = area;
    }

    public Imovel() {
    }
    
    public static class ImovelBuilder {
        private String tipo;
        private String status;
        private float valor;         
        private String rua;
        private String bairro;
        private String cidade;
        private String numero;
        private String quartosComSuite;
        private String quartosSemSuite;
        private String andares;
        private String banheiros;
        private float area;

        public ImovelBuilder() {
        }
        
        public ImovelBuilder tipo(String tipo){
            this.tipo = tipo;
            return this;
        }
        
        public ImovelBuilder status(String status){
            this.status = status;
            return this;
        }
        
        public ImovelBuilder valor(float valor){
            this.valor = valor;
            return this;
        }
        
        public ImovelBuilder rua(String rua){
            this.rua = rua;
            return this;
        }
        
        public ImovelBuilder bairro(String bairro){
            this.bairro = bairro;
            return this;
        }
        
        public ImovelBuilder cidade(String cidade){
            this.cidade = cidade;
            return this;
        }
        
        public ImovelBuilder numero(String numero){
            this.numero = numero;
            return this;
        }
        
        public ImovelBuilder quartosComSuite(String quartosComSuite){
            this.quartosComSuite = quartosComSuite;
            return this;
        }
        
        public ImovelBuilder quartosSemSuite(String quartosSemSuite){
            this.quartosSemSuite = quartosSemSuite;
            return this;
        }
        
        public ImovelBuilder andares(String andares){
            this.andares = andares;
            return this;
        }
        
        public ImovelBuilder banheiros(String banheiros){
            this.banheiros = banheiros;
            return this;
        }
        
        public ImovelBuilder area(float area){
            this.area = area;
            return this;
        }
        
        public Imovel criarImovel(){
            return new Imovel(tipo,status,valor,rua,bairro,cidade,numero,quartosComSuite,
                    quartosSemSuite,andares,banheiros,area) {};
        }
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getQuartosComSuite() {
        return quartosComSuite;
    }

    public void setQuartosComSuite(String quartosComSuite) {
        this.quartosComSuite = quartosComSuite;
    }

    public String getQuartosSemSuite() {
        return quartosSemSuite;
    }

    public void setQuartosSemSuite(String quartosSemSuite) {
        this.quartosSemSuite = quartosSemSuite;
    }

    public String getAndares() {
        return andares;
    }

    public void setAndares(String andares) {
        this.andares = andares;
    }

    public String getBanheiros() {
        return banheiros;
    }

    public void setBanheiros(String banheiros) {
        this.banheiros = banheiros;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public int getImovelId() {
        return ImovelId;
    }

    public void setImovelId(int ImovelId) {
        this.ImovelId = ImovelId;
    }
    
}
